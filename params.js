const params = {
    db: {
        host: 'localhost',
        port: '27017',
        dbName: 'red-tetris'
    },
    server: {
        host: 'localhost',
        port: 5000,
        url: 'localhost:5000'
    }
};

export default params;
