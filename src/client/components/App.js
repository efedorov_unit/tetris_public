import React, { Component } from 'react';
import PropTypes            from 'prop-types';

import Spectre      from '../components/spectre';
import BoardGame    from './../containers/BoardGame';
import Room         from './../containers/Room';
import ServerPage   from './../containers/ServerPage';

import config from './../../../params.js';

import './style.scss';

export default class App extends Component {
    static propTypes = {
        room            : PropTypes.object,
        info            : PropTypes.object,
        error           : PropTypes.string,
        otherPlayers    : PropTypes.object,
        board           : PropTypes.object,
        router          : PropTypes.object,
        location        : PropTypes.object
    }

    state = {
        page: 'main'
    }

    handleBtnClick = (page) => {
        this.props.router.push('/');

        this.setState({ page });
    }

    render() {
        const {
            room,
            info,
            error,
            otherPlayers,
            board,
            router
        } = this.props;
        const { page } = this.state;
        const data = info && { name: info.getIn([ 'name' ]), room: info.getIn([ 'room' ]) };

        const errorCondition = (error || !data);
        const roomCondition = !errorCondition && room && !room.getIn([ 'isStarted' ]);
        const gameCondition = !errorCondition && room && room.getIn([ 'isStarted' ]);
        const gameMode = otherPlayers && otherPlayers.size >= 1 ? 'Multiplayer' : 'Single mode';

        return (
            <div className='container'>
                <header>
                    <h1>
                        <a href={`http://${config.server.host}:8080`}>{'Red-Tetris'}</a>
                    </h1>
                    {
                        roomCondition || gameCondition ? null :
                        <div>
                            <button className='button' onClick={this.handleBtnClick.bind(null, 'join')}>
                                {'Join room'}
                            </button>
                            <button className='button' onClick={this.handleBtnClick.bind(null, 'create')}>
                                {'Create room'}
                            </button>
                        </div>
                    }
                </header>
                <div className='content'>
                    {
                        roomCondition || gameCondition || page !== 'main'  ? null :
                        <div className='gif-background' />
                    }
                    {
                        roomCondition &&
                        (<div className='game-mode'>
                            <h1>{gameMode}</h1>
                        </div>)
                    }
                    <div className='center'>
                        {
                            !roomCondition && !gameCondition &&
                                <ServerPage {...this.props} router={router} page={page} />
                        }
                    </div>
                    {
                        otherPlayers && (
                        <div className='spectre--container'>
                            {otherPlayers.size >= 1 && (<Spectre player={otherPlayers.get(0)} board={board} />)}
                            {otherPlayers.size >= 2 && (<Spectre player={otherPlayers.get(1)} board={board} />)}
                            {otherPlayers.size >= 3 && (<Spectre player={otherPlayers.get(2)} board={board} />)}
                            {otherPlayers.size >= 4 && (<Spectre player={otherPlayers.get(3)} board={board} />)}
                        </div>)
                    }
                    {
                        roomCondition && <Room />
                    }
                    {
                        gameCondition && <BoardGame />
                    }
                </div>
            </div>
        );
    }
}
