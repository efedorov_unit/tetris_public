import React, { Component } from 'react';
import PropTypes            from 'prop-types';
import Modal                from 'react-modal';

import './style.scss';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        background            : '',
        border                : '',
        transform             : 'translate(-50%, -50%)'
    }
};

export default class ServerPage extends Component {
    static propTypes = {
        router      : PropTypes.object,
        getGames    : PropTypes.func,
        openModal   : PropTypes.func,
        currentRoom : PropTypes.string,
        games       : PropTypes.oneOfType([
            PropTypes.array,
            PropTypes.object
        ]),
        modal       : PropTypes.bool,
        page        : PropTypes.string
    }

    async componentWillMount() {
        await this.props.getGames();
    }

    handleNewPlayerFormSubmit = () => {
        const {
            router,
            openModal,
            currentRoom,
            modal
        } = this.props;
        const Name = document.getElementById('Name').value;

        openModal({ modal: !modal, currentRoom: null });
        router.push(`#${currentRoom}[${Name}]`);
    }

    handleNewServerFormSubmit = () => {
        const {
            router
        } = this.props;
        const hostName = document.getElementById('hostName').value;
        const roomName = document.getElementById('roomName').value;

        if (hostName !== '' && roomName !== '') {
            router.push(`#${roomName}[${hostName}]`);
        }
    }

    handleRequestClose = () => {
        const { modal, openModal } = this.props;

        openModal({ modal: !modal, currentRoom: null });
    }

    renderRoomsPage() {
        const { games, openModal, modal } = this.props;

        return games && games.size ? games.map((p, i) => {
            const players = p.get('players').toJS();
            let str = p.get('room');

            if (str.length >= 10) {
                str = `${str.slice(0, 10)}...`;
            }

            if (!p.get('isStarted')) {
                return (
                    <button
                        className='notStarted'
                        key={i}
                        onClick={openModal.bind(this, {
                            modal: !modal,
                            currentRoom: p.get('room')
                        })}
                    >
                        <p>{str}</p>
                        <br />
                        <p>{`${players.length}/5`} </p>
                    </button>
                );
            }

            return (
                <button
                    className='disabled asStarted'
                    key={i}
                >
                    <p>{str}</p>
                    <br />
                    <p>{`${players.length}/5`} </p>
                </button>
            );
        }) : <h1>No rooms yet</h1>;
    }

    render() {
        const {
            games,
            modal,
            page
        } = this.props;
        const roomsBlock = this.renderRoomsPage();

        return (
            <div>
                <Modal
                    isOpen={modal}
                    style={customStyles}
                    onRequestClose={this.handleRequestClose}
                    contentLabel='Name Modal'
                >
                    <form
                        className='formModal'
                        onSubmit={this.handleNewPlayerFormSubmit}
                    >
                        <input
                            className='inputText'
                            type='text'
                            id='Name'
                            placeholder='Name'
                        />
                        <input
                            className='input'
                            type='submit'
                            value='GO'
                        />
                    </form>
                </Modal>
                <div>
                    {
                        games && page === 'join' ?
                            <div className='join-game-container'>
                                <h1>Join room</h1>
                                {roomsBlock}
                            </div> : null
                    }
                </div>
                {
                    page === 'create' &&
                        <div>
                            <h1 style={{ textAlign: 'center', marginBottom: '20px' }}>
                                Create your room
                            </h1>
                            <div className='form'>
                                <input
                                    className='inputText'
                                    type='text'
                                    id='hostName'
                                    placeholder='Host Name'
                                />
                                <input
                                    className='inputText'
                                    type='text'
                                    id='roomName'
                                    placeholder='Room Name'
                                />
                                <button className='input' type='submit' onClick={this.handleNewServerFormSubmit}>
                                    GO
                                </button>
                            </div>
                        </div>
                }
            </div>
        );
    }
}
