import { assert }     from 'chai';
import { fromJS }     from 'immutable';
import isEqual        from 'lodash/isEqual';
import {
  isPossible, lineDestroyer, isPiece, rotate, pieceToBoard, updateSpectre, isGameLost
}                     from '../src/client/helper.js';

const board = [
    [
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' }
    ],
    [
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' }
    ],
    [
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' }
    ],
    [
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' }
    ],
    [
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' },
      { color: 'white' }
    ],
    [
      { color: 'red' },
      { color: 'red' },
      { color: 'red' },
      { color: 'red' },
      { color: 'red' },
      { color: 'red' },
      { color: 'red' },
      { color: 'white' }
    ]
];

const boardTwo = [
    [
      { color: 'white' },
      { color: 'red' }
    ],
    [
      { color: 'white' },
      { color: 'white' }
    ]
];

const shape  = [
  [0, 0, 0, 0],
  [0, 1, 1, 0],
  [0, 1, 1, 0],
  [0, 0, 0, 0]
];

const spectre = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

describe('front helper', () => {
    describe('isGameLost', () => {
        it('should be false if lost', done => {
            const page = isGameLost(fromJS(board));

            assert.equal(page, false);
            done();
        });
        it('should be true if lost', done => {
            const page = isGameLost(fromJS(boardTwo));

            assert.equal(page, true);
            done();
        });
    });
    describe('isPossible', () => {
        it('should be false if not possible', done => {
            const page = isPossible(fromJS(board), shape, 2, 2);

            assert.equal(page, true);
            done();
        });
        it('should be true if lost', done => {
            const page = isPossible(fromJS(board), shape, 5, 5);

            assert.equal(page, false);
            done();
        });
    });
    describe('lineDestroyer', () => {
        it('should be false if not possible', done => {
            const page = lineDestroyer(fromJS(board), null, 1111, 22222);

            assert.equal(page, false);
            done();
        });
    });

    describe('isPiece', () => {
        it('should be false if not possible', done => {
            const page = isPiece(1, 1, shape, 2, 2);

            assert.equal(page, false);
            done();
        });
        it('should be true if  possible', done => {
            const page =  isPiece(2, 2, shape, 0, 0);

            assert.equal(page, true);
            done();
        });
    });
    describe('Rotate', () => {
        it('should be rotatation', done => {
            const page = rotate(shape);

            assert.equal(true, isEqual(page, shape));
            done();
        });
    });
    describe('pieceToBoard', () => {
        it('should be rotatation', done => {
            const page = pieceToBoard(fromJS(board), shape, 10, 10, 'red');

            assert.equal(true, isEqual(page, board));
            done();
        });
    });
    describe('updateSpectre', () => {
        it('should be rotatation', done => {
            const page = updateSpectre(fromJS(board), spectre);

            assert.equal(true, isEqual(page, [0, 0, 0, 0, 0, 0, 0, 0, 21, 21]));
            done();
        });
    });
});
