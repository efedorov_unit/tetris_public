import React          from 'react';
import { fromJS }     from 'immutable';
import ReactTestUtils from 'react-addons-test-utils';
import { assert }     from 'chai';

import Spectre    from '../src/client/components/spectre';
import ServerPage from '../src/client/components/serverPage';
import Room       from '../src/client/components/room';
import App        from '../src/client/components/App.js';

function emptyFunction() {
    return null;
}

const state = {
    player: fromJS({
        spectre: [2, 0, 0, 2, 0, 10, 15, 14],
        name: 'test',
        winner: 'false',
        looser: 'false'
    }),
    board:fromJS([
        [ { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' },
            { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' } ],
        [ { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' },
            { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' } ],
        [ { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' },
            { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' } ],
        [ { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' },
            { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' } ],
        [ { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' },
            { color: 'white' }, { color: 'white' }, { color: 'white' }, { color: 'white' } ],
        [ { color: 'red' }, { color: 'red' }, { color: 'red' }, { color: 'red' },
            { color: 'red' }, { color: 'red' }, { color: 'red' }, { color: 'white' } ]
    ]),
    games: fromJS([ {
        players: ['ooo', 'ooo'],
        room: 'test',
        isStarted: false
    } ]),
    games1: fromJS([ {
        players: ['ooo', 'ooo'],
        room: 'test',
        isStarted: true
    } ]),
    games2: fromJS([ {
        players: ['ooo', 'ooo'],
        room: 'teoooooooooooooooooooooooooooooot',
        isStarted: true
    } ]),
    players: fromJS([ {
        spectre: [2, 0, 0, 2, 0, 10, 15, 14],
        name: 'test',
        winner: 'false',
        looser: 'false',
        isVisitor: false
    } ]),
    players1: fromJS([ {
        spectre: [2, 0, 0, 2, 0, 10, 15, 14],
        name: 'test',
        winner: 'false',
        looser: 'false',
        isVisitor: true
    } ])
};

describe('Components', () => {
    describe('Spectre', () => {
        it('should be a div after render', done => {
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <Spectre
                    player={state.player}
                    board={state.board}
                />
            );

            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });
    });
    describe('ServerPage', () => {
        it('should be a div after render', done => {
            const test = {
                router: null,
                getGames: null,
                openModal: emptyFunction,
                currentRoom: null,
                games: state.games,
                modal: true
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <ServerPage {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                router: null,
                getGames: null,
                openModal: null,
                currentRoom: null,
                games: null,
                modal: false
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <ServerPage {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                router: null,
                getGames: null,
                openModal: null,
                currentRoom: null,
                games: state.games1,
                modal: true
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <ServerPage {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                router: null,
                getGames: null,
                openModal: null,
                currentRoom: null,
                games: state.games2,
                modal: true
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <ServerPage {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });
    });

    describe('App', () => {
        it('should be a div after render', done => {
            const test = {
                room: null,
                info: fromJS({ name: 'heee', room: 'oeuoeu' }),
                newPlayer: null,
                error: 'heteheh',
                otherPlayers: null,
                board: state.board,
                router: null
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <App {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                room: null,
                info: fromJS({ name: 'heee', room: 'oeuoeu' }),
                newPlayer: null,
                error: null,
                otherPlayers: null,
                board: state.board,
                router: null
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <App {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                room: fromJS(),
                info: fromJS({ name: 'heee', room: 'oeuoeu' }),
                newPlayer: null,
                error: null,
                otherPlayers: null,
                board: state.board,
                router: null
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <App {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                room: fromJS({ isStarted: true }),
                info: fromJS({ name: 'heee', room: 'oeuoeu' }),
                newPlayer: null,
                error: null,
                otherPlayers: null,
                board: state.board,
                router: null
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <App {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                room: fromJS({ isStarted: false }),
                info: fromJS({ name: 'heee', room: 'oeuoeu' }),
                newPlayer: null,
                error: null,
                otherPlayers: null,
                board: state.board,
                router: null
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <App {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });
    });

    describe('Room', () => {
        it('should be a div after render', done => {
            const test = {
                room: fromJS({ isStarted: false }),
                info: fromJS({ name: 'test', room: 'oeuoeu' }),
                players: state.players,
                startGame: emptyFunction
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <Room {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });

        it('should be a div after render', done => {
            const test = {
                room: fromJS({ isStarted: false }),
                info: fromJS({ name: 'test', room: 'oeuoeu' }),
                players: state.players1,
                startGame: emptyFunction
            };
            const renderer = ReactTestUtils.createRenderer();

            renderer.render(
                <Room {...test} />
            );
            const result = renderer.getRenderOutput();

            assert.equal(result.type, 'div');
            done();
        });
    });
});
