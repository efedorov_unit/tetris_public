import { assert } from 'chai';
import mongoose   from 'mongoose';
import io         from 'socket.io-client';

const socketURL = 'http://localhost:5000';
let socket;

function clearDB() {
    for (const i in mongoose.connection.collections) {
        if (mongoose.connection.collections.hasOwnProperty(i)) {
            mongoose.connection.collections[i].remove();
        }
    }
}

describe('Event', () => {
    after(() => clearDB());

    beforeEach(done => {
        socket = io.connect(socketURL);
        socket.on('connect', () => {
            done();
        });
    });

    describe('Socket: create a player', () => {
        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }

            done();
        });

        it('should be OK create player', done => {
            socket.on('action', (res) => {
                assert.equal(res.data.room, 'the_first_time');
                done();
            });
            socket.emit('newPlayer', { room: 'the_first_time', name: 'testo' });
        });
        it('should be ERROR create player', done => {
            socket.on('action', (res) => {
                assert.equal(res.data, 'name already taken');
                done();
            });
            socket.emit('newPlayer', { room: 'the_first_time', name: 'testo' });
        });
    });

    describe('Socket: get games', () => {
        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });

        it('should be OK get games', done => {
            socket.on('action', (res) => {
                assert.equal(res.data.length, 1);
                done();
            });
            socket.emit('getGames', {});
        });
    });


    describe('Socket: loosse a game', () => {
        let gameId;
        let playerId;

        beforeEach(done => {
            socket.on('action', (res) => {
                if (res.type === 'NEW_PLAYER') {
                    gameId = res.data.id;
                    playerId = res.data.players[0]._id;
                    done();
                }
            });
            socket.emit('newPlayer', { room: 'the_111', name: 'testo' });
        });

        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });

        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });
        it('should be OK loosse game', done => {
            socket.on('action', (res) => {
                assert.equal(res.data.players[0].looser, true);
                done();
            });
            socket.emit('loseGame', { playerId, gameId });
        });
    });

    describe('Socket: Give a malus', () => {
        let gameId;
        let playerId;
        let malus;

        beforeEach(done => {
            socket.on('action', (res) => {
                if (res.type === 'NEW_PLAYER') {
                    gameId = res.data.id;
                    playerId = res.data.players[0]._id;
                    malus = 2;
                    done();
                }
            });
            socket.emit('newPlayer', { room: 'the_11133', name: 'testo' });
        });

        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });

        it('should be OK give malus', done => {
            socket.on('action', (res) => {
                assert.equal(res.type, 'GIVE_MALUS');
                assert.equal(res.data.malus, 0);
                done();
            });
            socket.emit('giveMalus', { playerId, gameId, malus });
        });
    });

    describe('Socket: Restart a game', () => {
        let gameId;

        beforeEach(done => {
            socket.on('action', (res) => {
                if (res.type === 'NEW_PLAYER') {
                    gameId = res.data.id;
                    done();
                }
            });
            socket.emit('newPlayer', { room: 'the_111332', name: 'testo' });
        });

        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });
        it('should be OK Restart a game', done => {
            socket.on('action', (res) => {
                assert.equal(res.type, 'RESTART_GAME');
                assert.equal(res.data.room, 'the_111332');
                assert.equal(res.data.isOver, false);
                done();
            });
            socket.emit('restartGame', { gameId });
        });
    });

    describe('Socket: Start a game', () => {
        let gameId;

        beforeEach(done => {
            socket.on('action', (res) => {
                if (res.type === 'NEW_PLAYER') {
                    gameId = res.data.id;
                }
                if (res.type === 'NEW_PLAYER') {
                    gameId = res.data.id;
                    done();
                }
            });
            socket.emit('newPlayer', { room: 'the_1113335', name: 'testo' });
        });

        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });
        it('should be OK Start  a game', done => {
            socket.on('action', (res) => {
                assert.equal(res.type, 'START_GAME');
                assert.equal(res.data.room, 'the_1113335');
                assert.equal(res.data.isOver, false);
                assert.equal(res.message, 'Game successfully started');
                done();
            });
            socket.emit('startGame', { gameId });
        });
    });

    describe('Socket: Next turn', () => {
        let gameId;
        let playerId;
        let spectre;

        beforeEach(done => {
            socket.on('action', (res) => {
                if (res.type === 'NEW_PLAYER') {
                    gameId = res.data.id;
                    playerId = res.data.players[0]._id;
                    spectre = res.data.players[0].spectre;
                    done();
                }
            });
            socket.emit('newPlayer', { room: 'the_1113344', name: 'testo' });
        });

        after((done) => {
            if (socket.connected) {
                socket.disconnect();
            }
            done();
        });

        it('should be OK give malus', done => {
            socket.on('action', (res) => {
                assert.equal(res.type, 'NEXT_TURN');
                assert.equal(res.data.updatedGame.isOver, false);
                done();
            });

            socket.emit('nextTurn', { playerId, gameId, spectre });
        });
    });
});
