import { assert }         from 'chai';
import isEqual            from 'lodash/isEqual';
import activation         from '../src/client/actions/activation';
import clearMessage       from '../src/client/actions/clearMessage';
import disactivation      from '../src/client/actions/disactivation';
import disconnect         from '../src/client/actions/disconnect';
import getGames           from '../src/client/actions/getGames';
import giveMalus          from '../src/client/actions/giveMalus';
import loseGame           from '../src/client/actions/loseGame';
import moveLine           from '../src/client/actions/moveLine';
import newPlayer          from '../src/client/actions/newPlayer';
import nextTurn           from '../src/client/actions/nextTurn';
import openModal          from '../src/client/actions/openModal';
import posePiece          from '../src/client/actions/posePiece';
import restartGame        from '../src/client/actions/restartGame';
import startGame          from '../src/client/actions/startGame';
import startInterval      from '../src/client/actions/startInterval';
import tetriMove          from '../src/client/actions/tetriMove';
import tetriRotate        from '../src/client/actions/tetriRotate';

describe('Components', () => {
    describe('Acion', () => {
        it('should be true when actions is test', done => {
            const page = activation();

            assert.equal(true, isEqual(page, { type: 'client/activation' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = clearMessage();

            assert.equal(true, isEqual(page, { type: 'client/clearMessage' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = disactivation();

            assert.equal(true, isEqual(page, { type: 'client/disactivation' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = disconnect('test');

            assert.equal(true, isEqual(page, { type: 'server/disconnect', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = getGames('test');

            assert.equal(true, isEqual(page, { type: 'server/getGames', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = giveMalus('test');

            assert.equal(true, isEqual(page, { type: 'server/giveMalus', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = loseGame('test');

            assert.equal(true, isEqual(page, { type: 'server/loseGame', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = moveLine('test');

            assert.equal(true, isEqual(page, { type: 'client/moveLine', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = newPlayer('test');

            assert.equal(true, isEqual(page, { type: 'server/newPlayer', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = nextTurn('test');

            assert.equal(true, isEqual(page, { type: 'server/nextTurn', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = openModal('test');

            assert.equal(true, isEqual(page, { type: 'client/openModal', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = posePiece('test');

            assert.equal(true, isEqual(page, { type: 'client/posePiece', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = restartGame('test');

            assert.equal(true, isEqual(page, { type: 'server/restartGame', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = startGame('test');

            assert.equal(true, isEqual(page, { type: 'server/startGame', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = startInterval('test');

            assert.equal(true, isEqual(page, { type: 'client/startInterval', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = tetriMove('test');

            assert.equal(true, isEqual(page, { type: 'client/tetriMove', data: 'test' }));
            done();
        });
        it('should be true when actions is test', done => {
            const page = tetriRotate('test');

            assert.equal(true, isEqual(page, { type: 'client/tetriRotate', data: 'test' }));
            done();
        });
    });
});
